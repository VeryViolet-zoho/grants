import os
import subprocess
import regex
import time
import click
import random
import sys
import json
import glob
import logging
import uuid
from shutil import copy


def GetGrantsRE(RawDocsData, donor):
    grantsRE = '('
    first = True
    for Prefix in RawDocsData["prefixes"]:
        for Postfix in RawDocsData["postfixes"]:
            addon = Prefix["text"] + donor + Postfix["text"]
            if first:
                first = False
            else:
                grantsRE += '|'
            grantsRE = grantsRE + addon
    grantsRE += ')([\p{Z}\p{C}]+)([\p{Pd}\p{L}\p{N}]+)([\p{Z}\p{P}\p{C}]+)'
    grantsRE = grantsRE.replace(' ', '[\p{Z}\p{C}]+')

    return regex.compile(grantsRE)


def GetGrantName(filename, RE):
    grantname = None
    tmp_file_name = str(uuid.uuid4())
    try:
        fulltext = ''

        subprocess.call(['pdftotext', filename, tmp_file_name],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
        #os.system('pdftotext -layout %s %s' % (filename, tmp_file_name))

        with open(tmp_file_name, encoding='utf-8') as f:
            fulltext = f.read()

        m = RE.search(fulltext)
        if m:
            found = m.group(3)
            grantname = found
    finally:
        if os.path.exists(tmp_file_name):
            os.remove(tmp_file_name)
        return grantname


