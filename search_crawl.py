import os
import time
import click
import random
import sys
import json
from twisted.internet import reactor, defer
import scrapy
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from pyvirtualdisplay import Display



runner = CrawlerRunner(get_project_settings())


def CreateFolders(SearchesData, baseSearchFolder):
    if not os.path.exists(baseSearchFolder):
        os.mkdir(baseSearchFolder)
    for S in SearchesData["searches"]:
        searchFolder = os.path.join(baseSearchFolder, S["name"])
        if not os.path.exists(searchFolder):
            os.mkdir(searchFolder)

# def CrawlOneCombination(phrase, keywords, doctype, destination):
#     process.crawl('docs', phrase=phrase, keywords=keywords, doctype=doctype, destination=destination)
#     process.start()  # the script will block here until the crawling is finished


@defer.inlineCallbacks
def crawl():
    click.secho('--> Reading configuration...', fg='green')
    with open("./searches.json") as raw_docs_config_file:
        SearchesData = json.load(raw_docs_config_file)

    baseSearchFolder = "./searches"

    click.secho('--> Creating folders...', fg='green')
    CreateFolders(SearchesData, baseSearchFolder)

    for Search in SearchesData["searches"]:
        click.secho('-->   Processing search \"%s\"' % Search["name"], fg='yellow')
        destinationFolder = os.path.join(baseSearchFolder,Search["name"])
        yield runner.crawl('docs', phrase=Search["phrase"], keywords=Search["keywords"], doctype="pdf", destination=destinationFolder)


@click.command()
def trainmain():

    click.secho('\n\nSearch docs crawler v0.1\n\n', fg='green')

    display = Display(visible=0, size=(800, 600))
    display.start()

    crawl()
    reactor.run()

    click.secho('--> Done.', fg='green')

if __name__ == '__main__':
    trainmain()


