import os
import subprocess
import regex
import time
import click
import random
import sys
import json
import glob
import logging
import uuid
from shutil import copy
from grantname import GetGrantsRE,GetGrantName

def CreateFolders(RawDocsData, baseSortedFolder):
    if not os.path.exists(baseSortedFolder):
        os.mkdir(baseSortedFolder)
    for D in RawDocsData["donors"]:
        donorFolder = baseSortedFolder + "/" + D["name"]
        if not os.path.exists(donorFolder):
            os.mkdir(donorFolder)


def ProcessDonor(source, target, RE):
    files = glob.glob(source + '/*.pdf')
    for f in files:
        gn = GetGrantName(f, RE)
        if not gn:
            continue
        click.secho('-->   Found grant \"%s\" document.' % gn, fg='cyan')
        target_grant_dir = target + '/' + gn
        if not os.path.exists(target_grant_dir):
            os.mkdir(target_grant_dir)
        copy(f, target_grant_dir)
        copy(f.replace('.pdf','.info'), target_grant_dir)


@click.command()
def trainmain():
    logging.getLogger().propagate = False
    logging.getLogger().setLevel(logging.ERROR)
    click.secho('\n\nGrants sort raw docs v0.1\n\n', fg='green')

    click.secho('--> Reading configuration...', fg='green')

    with open("./docs.json") as raw_docs_config_file:
        RawDocsData = json.load(raw_docs_config_file)

    baseRawFolder = "./raw"
    baseSortedFolder = "./sorted"

    CreateFolders(RawDocsData,baseSortedFolder)

    for Donor in RawDocsData["donors"]:
        click.secho('-->   Processing donor \"%s\"' % Donor["name"], fg='yellow')
        RE = GetGrantsRE(RawDocsData,Donor["name"])
        sourceFolder = baseRawFolder + "/" + Donor["name"]
        destinationFolder = baseSortedFolder + "/" + Donor["name"]
        ProcessDonor(sourceFolder, destinationFolder, RE)




    # path = get_file('nietzsche.txt', origin="https://s3.amazonaws.com/text-datasets/nietzsche.txt")
    # text = open(path).read().lower()
    # click.secho('--> Source corpus length: %d' % len(text), fg='green')
    #
    # chars = set(text)
    # click.secho('--> Total chars: %d' % len(chars))
    # char_indices = dict((c, i) for i, c in enumerate(chars))
    # indices_char = dict((i, c) for i, c in enumerate(chars))
    #
    # # cut the text in semi-redundant sequences of maxlen characters
    # maxlen = 40
    # step = 3
    # sentences = []
    # next_chars = []
    # for i in range(0, len(text) - maxlen, step):
    #     sentences.append(text[i: i + maxlen])
    #     next_chars.append(text[i + maxlen])
    #
    # click.secho('--> Formed sequences: %d' % len(sentences), fg='green')
    #
    # click.secho('--> Vectorization...')
    # X = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
    # y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
    # for i, sentence in enumerate(sentences):
    #     for t, char in enumerate(sentence):
    #         X[i, t, char_indices[char]] = 1
    #     y[i, char_indices[next_chars[i]]] = 1
    #
    # click.secho('--> Done.', fg='green')
    #
    # # build the model: 2 stacked LSTM
    # click.secho('--> Building a model...', fg='green')
    # model = Sequential()
    # model.add(LSTM(512, return_sequences=True, input_shape=(maxlen, len(chars))))
    # model.add(Dropout(0.2))
    # model.add(LSTM(512, return_sequences=False))
    # model.add(Dropout(0.2))
    # model.add(Dense(len(chars)))
    # model.add(Activation('softmax'))
    #
    # model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
    #
    # click.secho('--> Done.', fg='green')
    #
    # click.secho('--> Training model...', fg='green')
    #
    # # train the model, output generated text after each iteration
    # for iteration in range(1, 60):
    #     click.secho('--> Iteration %d.' % iteration, fg='green')
    #
    #     model.fit(X, y, batch_size=128, nb_epoch=1)
    #
    #     start_index = random.randint(0, len(text) - maxlen - 1)
    #
    #     for diversity in [0.2, 0.5, 1.0, 1.2]:
    #         click.secho('--> diversity: %f' % diversity, fg='green')
    #
    #         generated = ''
    #         sentence = text[start_index: start_index + maxlen]
    #         generated += sentence
    #         click.secho('--> Generating with seed: %s\n' % sentence, fg='green')
    #
    #         for i in range(200):
    #             x = np.zeros((1, maxlen, len(chars)))
    #             for t, char in enumerate(sentence):
    #                 x[0, t, char_indices[char]] = 1.
    #
    #             preds = model.predict(x, verbose=0)[0]
    #             next_index = sample(preds, diversity)
    #             next_char = indices_char[next_index]
    #
    #             generated += next_char
    #             sentence = sentence[1:] + next_char
    #
    #         click.secho(generated, fg='white')

    click.secho('--> Done.', fg='green')

if __name__ == '__main__':
    trainmain()


