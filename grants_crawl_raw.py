import os
import time
import click
import random
import sys
import json
from twisted.internet import reactor, defer
import scrapy
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from pyvirtualdisplay import Display





runner = CrawlerRunner(get_project_settings())


def CreateFolders(RawwDocsData, baseRawFolder):
    if not os.path.exists(baseRawFolder):
        os.mkdir(baseRawFolder)
    for D in RawwDocsData["donors"]:
        donorFolder = baseRawFolder + "/" + D["name"]
        if not os.path.exists(donorFolder):
            os.mkdir(donorFolder)

# def CrawlOneCombination(phrase, keywords, doctype, destination):
#     process.crawl('docs', phrase=phrase, keywords=keywords, doctype=doctype, destination=destination)
#     process.start()  # the script will block here until the crawling is finished


@defer.inlineCallbacks
def crawl():
    click.secho('--> Reading configuration...', fg='green')
    with open("./docs.json") as raw_docs_config_file:
        RawDocsData = json.load(raw_docs_config_file)

    baseRawFolder = "./raw"

    click.secho('--> Creating folders...', fg='green')
    CreateFolders(RawDocsData, baseRawFolder)

    for Donor in RawDocsData["donors"]:
        click.secho('-->   Processing donor \"%s\"' % Donor["name"], fg='yellow')
        for Prefix in RawDocsData["prefixes"]:
            click.secho('-->     Processing prefix \"%s\"' % Prefix["text"], fg='yellow')
            for Postfix in RawDocsData["postfixes"]:
                click.secho('-->       Processing postfix \"%s\"' % Postfix["text"], fg='yellow')
                for Doctype in RawDocsData["doctypes"]:
                    click.secho('-->         Processing doctype \"%s\"' % Doctype["name"], fg='yellow')
                    destinationFolder = baseRawFolder + "/" + Donor["name"]
                    phrase = Prefix["text"] + Donor["name"] + Postfix["text"]
                    click.secho('-->           crawling ... ', fg='red')
                    yield runner.crawl('docs', phrase=phrase, keywords=RawDocsData["keywords"], doctype=Doctype["name"], destination=destinationFolder)


@click.command()
def trainmain():

    click.secho('\n\nGrants raw docs crawler v0.1\n\n', fg='green')

    display = Display(visible=0, size=(800, 600))
    display.start()

    crawl()
    reactor.run()

    # path = get_file('nietzsche.txt', origin="https://s3.amazonaws.com/text-datasets/nietzsche.txt")
    # text = open(path).read().lower()
    # click.secho('--> Source corpus length: %d' % len(text), fg='green')
    #
    # chars = set(text)
    # click.secho('--> Total chars: %d' % len(chars))
    # char_indices = dict((c, i) for i, c in enumerate(chars))
    # indices_char = dict((i, c) for i, c in enumerate(chars))
    #
    # # cut the text in semi-redundant sequences of maxlen characters
    # maxlen = 40
    # step = 3
    # sentences = []
    # next_chars = []
    # for i in range(0, len(text) - maxlen, step):
    #     sentences.append(text[i: i + maxlen])
    #     next_chars.append(text[i + maxlen])
    #
    # click.secho('--> Formed sequences: %d' % len(sentences), fg='green')
    #
    # click.secho('--> Vectorization...')
    # X = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
    # y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
    # for i, sentence in enumerate(sentences):
    #     for t, char in enumerate(sentence):
    #         X[i, t, char_indices[char]] = 1
    #     y[i, char_indices[next_chars[i]]] = 1
    #
    # click.secho('--> Done.', fg='green')
    #
    # # build the model: 2 stacked LSTM
    # click.secho('--> Building a model...', fg='green')
    # model = Sequential()
    # model.add(LSTM(512, return_sequences=True, input_shape=(maxlen, len(chars))))
    # model.add(Dropout(0.2))
    # model.add(LSTM(512, return_sequences=False))
    # model.add(Dropout(0.2))
    # model.add(Dense(len(chars)))
    # model.add(Activation('softmax'))
    #
    # model.compile(loss='categorical_crossentropy', optimizer='rmsprop')
    #
    # click.secho('--> Done.', fg='green')
    #
    # click.secho('--> Training model...', fg='green')
    #
    # # train the model, output generated text after each iteration
    # for iteration in range(1, 60):
    #     click.secho('--> Iteration %d.' % iteration, fg='green')
    #
    #     model.fit(X, y, batch_size=128, nb_epoch=1)
    #
    #     start_index = random.randint(0, len(text) - maxlen - 1)
    #
    #     for diversity in [0.2, 0.5, 1.0, 1.2]:
    #         click.secho('--> diversity: %f' % diversity, fg='green')
    #
    #         generated = ''
    #         sentence = text[start_index: start_index + maxlen]
    #         generated += sentence
    #         click.secho('--> Generating with seed: %s\n' % sentence, fg='green')
    #
    #         for i in range(200):
    #             x = np.zeros((1, maxlen, len(chars)))
    #             for t, char in enumerate(sentence):
    #                 x[0, t, char_indices[char]] = 1.
    #
    #             preds = model.predict(x, verbose=0)[0]
    #             next_index = sample(preds, diversity)
    #             next_char = indices_char[next_index]
    #
    #             generated += next_char
    #             sentence = sentence[1:] + next_char
    #
    #         click.secho(generated, fg='white')

    click.secho('--> Done.', fg='green')

if __name__ == '__main__':
    trainmain()


