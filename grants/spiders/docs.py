import re
import os
import json
import hashlib
import random
import click
import socket
import time
from time import sleep
from urllib import request
from urllib.parse import urljoin, urlparse
from datetime import datetime
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.selector import Selector
from scrapy.spiders import Spider
from scrapy.utils.response import get_base_url
from pdfsearch import SearchForString
from selenium import webdriver

timeout = 60
socket.setdefaulttimeout(timeout)

class ResultString:
    download_error = 'download error'
    exception = 'exception'
    ok = 'downloaded'
    false = 'false'


class DocsSpider(Spider):
    name = 'docs'
    Hasher = hashlib.md5()
    base_url_fmt = 'http://scholar.google.com/scholar?hl=en&as_q=&as_epq={phrase}&as_oq={words}&as_eq=&as_nlo=&as_nhi=&lr=&as_qdr=all&as_sitesearch=&as_occt=any&safe=images&tbs=&as_filetype={doctype}&as_rights='

    phrase = None
    keywords = None
    doctype = None
    destination = None

    def __init__(self, *args, **kwargs):
        super(DocsSpider, self).__init__(*args, **kwargs)


        phrase = kwargs.get('phrase')
        keywords = kwargs.get('keywords')
        doctype = kwargs.get('doctype')
        destination = kwargs.get('destination')
        if not phrase:
            phrase = ""
        if not keywords:
            keywords = ""
        if not doctype:
            raise ValueError('No doctype given')
        if not destination:
            raise ValueError('No destination given')


    def create_webdriver(self, target_folder):
        options = webdriver.ChromeOptions()
        profile = {"plugins.plugins_list": [{"enabled": False, "name": "Chrome PDF Viewer"}],
                   "download.default_directory": target_folder}
        options.add_experimental_option("prefs", profile)
        chromeDriver = webdriver.Chrome(chrome_options=options)
        return chromeDriver

    def download_pdf(self, driver, pdf_url, folder):

#        click.secho('started')
        before_set = set(os.listdir(folder))

        driver.get(pdf_url)
        time.sleep(15)

#        click.secho('requested')

        after_set = set(os.listdir(folder))
        change = after_set - before_set

        if len(change) == 0:
            return None

#        click.secho('waiting')


        while next(iter(change)).find('.crdownload') != -1:
#            click.secho('...downloading...')
            time.sleep(3)
            after_set = set(os.listdir(folder))
            change = after_set - before_set

#        click.secho('finished')

        return next(iter(change))

    def start_requests(self):
        url =self.make_google_search_request()
        yield Request(url=url)

    def make_google_search_request(self):
        return self.base_url_fmt.format(phrase='+'.join(self.phrase.split()).strip('+'), words='+'.join(self.keywords.split()).strip('+'),
                   doctype='+'.join(self.doctype.split()).strip('+'))


    def parse(self, response):
        sel = Selector(response)
        google_search_links_list = sel.xpath('//div[@class="gs_or_ggsm"]/a/@href').extract()
#        google_search_links_list = [re.search('q=(.*)&sa', n).group(1) for n in google_search_links_list if
#                                       re.search('q=(.*)&sa', n)]

        random.shuffle(google_search_links_list)

        chromeDriver = self.create_webdriver(self.destination)

        for pdf in google_search_links_list:
            hash = hashlib.sha1(pdf.encode())
            fname = os.path.join(self.destination, hash.hexdigest() +'.' + self.doctype)
            inf_fname = self.destination + '/' + hash.hexdigest() + '.info'


            info = {}

            if os.path.exists(inf_fname):
                with open(inf_fname) as f:
                    info = json.load(f)
                    if info['result'] == ResultString.false or info['result'] == ResultString.ok:
                        click.secho('---# processed (skipped) : %s' % pdf, fg='cyan')
                        continue
            else:
                info['url'] = pdf
                info['downloaded'] = str(datetime.now())

            try:
                #click.secho('trying to download')
                original = self.download_pdf(chromeDriver, pdf, self.destination)

                if original is None:
                    info['result'] = ResultString.download_error
                else:
                    #click.secho('renaming %s to %s' %
                     #           (os.path.join(self.destination, original),fname))
                    os.rename(os.path.join(self.destination, original), fname)

                    info['result'] = ResultString.ok
                    #click.secho('downloaded. checking...')

                    if os.path.exists(fname) and self.doctype == 'pdf':
                        if self.phrase and not SearchForString(fname, self.phrase):
                            os.remove(fname)
                            info['result'] = ResultString.false
            except:
                info['result'] = ResultString.exception

            with open(inf_fname, 'w') as info_file:
                json.dump(info, info_file, indent=4)

            click.secho('---# processed (%s) : %s' % (info['result'], pdf), fg='cyan')

        chromeDriver.close()

        next_page = sel.xpath('//a[span[contains(@class,"gs_ico_nav_next")]]/@href')
        if next_page:
            url = self._build_absolute_url(response, next_page.extract()[0])
            yield Request(url=url, callback=self.parse)

    def _build_absolute_url(self, response, url):
         return urljoin(get_base_url(response), url)

# def _parse_url(href):
#     """
#     parse the website from anchor href.
#
#     for example:
#
#     >>> _parse_url(u'/url?q=http://www.getmore.com.hk/page18.php&sa=U&ei=Xmd_UdqBEtGy4AO254GIDg&ved=0CDQQFjAGODw&usg=AFQjCNH08dgfL10dJVCyQjfu_1DEyhiMHQ')
#     u'http://www.getmore.com.hk/page18.php'
#     """
#     queries = dict(parse_qsl(urlparse(href).query))
#     return queries.get('q', '')
#
# def _get_region(url):
#     """
#     get country code from the url.
#
#     >>> _get_region('http://scrapinghub.ie')
#     'ie'
#     >>> _get_region('http://www.astoncarpets.ie/contact.htm')
#     'ie'
#     """
#     netloc = urlparse(url)[1]
#     return netloc.rpartition('.')[-1]
