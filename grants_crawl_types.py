import os
import time
import click
import random
import exrex
import sys
import glob
import json
from twisted.internet import reactor, defer
import scrapy
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from grantname import GetGrantsRE,GetGrantName


runner = CrawlerRunner(get_project_settings())


@defer.inlineCallbacks
def crawl():
    click.secho('--> Reading configuration...', fg='green')
    with open("./docs.json") as raw_docs_config_file:
        RawDocsData = json.load(raw_docs_config_file)

    baseSortedFolder = "./sorted"

    for Donor in RawDocsData["donors"]:
        click.secho('-->   Processing donor \"%s\"' % Donor["name"], fg='yellow')
        for Type in Donor["types"]:
            Variants = exre

            grant_name = os.path.basename(d)
            if len(grant_name) < 7:
                continue
            for Doctype in RawDocsData["doctypes"]:
                click.secho('--> Crawling grant \"%s\" for %s...' % (grant_name, Doctype["name"]), fg='red')
                yield runner.crawl('docs', phrase=grant_name, keywords='', doctype=Doctype["name"], destination=d)


@click.command()
def trainmain():

    click.secho('\n\nGrants raw docs crawler v0.1\n\n', fg='green')

    crawl()
    reactor.run()



    click.secho('--> Done.', fg='green')

if __name__ == '__main__':
    trainmain()


